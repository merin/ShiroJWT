package com.hh.shirojwt.config;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

public class CustomRealm extends AuthorizingRealm {



    /**
     * 重写supports方法 使用JwtTokne代替AuthenticationToken
     */
    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JwtToken;
    }




    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken auth) throws AuthenticationException {
        System.out.println("身份验证");

/*        //http调用mkt用户中心验证用户
        boolean isOk = false;
        if(isOk){
            throw new AuthenticationException("登录失败");
        }*/
        String token = (String) auth.getCredentials();
        // 解密获得username，用于和数据库进行对比
        String username = JwtUtil.getUsername(token);
        System.out.println(username);
        if (username == null) {
            throw new AuthenticationException("token无效");
        }
        if (!JwtUtil.verify(token, "zwh", "ABC")) {
            throw new AuthenticationException("验证不通过");
        }
        return new SimpleAuthenticationInfo(token, token, "my_realm");
    }
}
