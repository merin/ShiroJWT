package com.hh.shirojwt.service;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


public interface LoginService {


    boolean login(String username, String password);

    String creatJwtTokne(String username);
}
